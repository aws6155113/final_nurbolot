terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.62.0"
    }
  }
}
provider "aws" {
  region = "us-east-1"
}
module "dev-vpc" {
  env                  = "dev"
  source               = "../child_modules/vpc/"
  vpc_cidr             = var.vpc_cidr_block
  public_subnet_cidrs  = slice(var.public_subnet_cidr_blocks, 13, 15)
  private_subnet_cidrs = slice(var.private_subnet_cidr_blocks, 13, 16)
}
module "dev-eks" {
  source          = "../child_modules/eks/"
  env             = "dev"
  vpc             = module.dev-vpc.vpc_id
  subnet          = module.dev-vpc.private_subnet_id
  cluster_name    = var.eks_name
  cluster_version = var.eks_version
  
  node_groups = {
    cluster1 = {
      node_group_name = "test"
      desired_size    = 3
      max_size        = 4
      min_size        = 2

      ami_type = "AL2_x86_64"
    },
  }
}

module "rds" {
    source = "../child_modules/rds"
    allocated_storage = 25
    engine = "postgres"
}
