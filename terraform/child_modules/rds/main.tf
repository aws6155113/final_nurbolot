
resource "aws_db_instance" "default" {
  allocated_storage   = var.allocated_storage
  db_name             = "nurbolotdb"
  engine              = var.engine
  engine_version      = "14.6"
  instance_class      = "db.t3.micro"
  username            = "nurbolot"
  password            = "nurbolot01"
  storage_type        = "gp2"
  publicly_accessible = true
  skip_final_snapshot = true
}